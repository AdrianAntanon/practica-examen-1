package ejercicios.repaso;

import java.util.Scanner;

public class Strings2_5 {

	public static final int palabras=5;
    public static void main(String[] args) {

        Scanner lector=new Scanner(System.in);

        String [] palabrasArray = new String [5];
        String [] segundoArray;
        String frase = " ";

        System.out.println("Introduzca 5 palabras en diferentes l�neas \n" +
                "Solo se coger� la primera palabra de cada frase");

        for(int i=0;i<palabras;i++) {
            System.out.println("Introduzca la frase de la l�nea n�"+(i+1)+" :");
            frase=lector.nextLine();
            
            segundoArray = frase.split(" ");
            palabrasArray[i]=segundoArray[0];
            
        }
        
        for(int j=4;j>=0;j--) {
            System.out.print(palabrasArray[j]);
            System.out.println(" ");
            
        }
        lector.close();

    }
}
