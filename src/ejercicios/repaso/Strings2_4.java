package ejercicios.repaso;

import java.util.Scanner;

public class Strings2_4 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		System.out.println("Introduce una frase y se le devolver�n las palabras en posiciones pares, por favor");
		
		String frase = lector.nextLine();
		int i=0;
		String [] array = frase.split(" ");
		
		System.out.println("La frase quedar�a de la siguiente forma: ");
		
		for(i=0;i<array.length;i++) {
			
			if((i+1)%2==0) {
				System.out.print(array[i]+" ");
			}
		}
		
		lector.close();

	}

}
