package ejercicios.repaso;

import java.util.Scanner;

public class Strings2_6 {

	public static void main(String[] args) {
		
		Scanner lector=new Scanner (System.in);
		String cadena="";
		
		System.out.println("Introduzca una frase, por favor: ");
		cadena=lector.nextLine().toLowerCase();
				
		char letraV='v', busqueda;
		int cantidadV=0, posicionInicio, posicionFinal;
		
		 for (int i = 0 ; i < cadena.length() ; i++) {
	            busqueda = cadena.charAt(i); 
	            if (busqueda == letraV) {
	                cantidadV++; 
	            }
	        }
		 
		 if(cantidadV > 1){
			 
         	posicionInicio=cadena.indexOf('v');
         	posicionFinal=cadena.lastIndexOf('v');
         	
         	String subcadena = cadena.substring(posicionInicio, posicionFinal+1); 
         	System.out.println(subcadena);
         	
         }else if(cantidadV == 1){
         	System.out.println("Solo hay una "+ letraV +" en el texto");
         }else{
        	 System.out.println("No hay ninguna "+ letraV +" en el texto");
         }
		lector.close();

	}

}
